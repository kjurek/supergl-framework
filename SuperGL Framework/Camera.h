#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <glm/glm.hpp>

class Camera
{
public:
	Camera() 
		:	m_position(0.0f, 0.0f, 0.0f),
			m_target(0.0f, 0.0f, 0.0f),
			m_up(0.0f, 1.0f, 0.0f),
			m_speed(0.1f), m_sensitivity(0.5f)
	{ }

	Camera(glm::vec3 position, glm::vec3 target, glm::vec3 up, float speed, float sensitivity)
		:	m_position(position), m_target(target), m_up(up), m_speed(speed),
			m_sensitivity(sensitivity)
	{ }

	~Camera() { }

	void setPosition(glm::vec3 position) { m_position = position; }
	void setTarget(glm::vec3 target) { m_target = target; }
	void setUp(glm::vec3 up) { m_up = up; }
	void setSpeed(float speed) { m_speed = speed; }
	void setSensitivity(float sensitivity) { m_sensitivity = sensitivity; }

	glm::vec3 getPosition() const { return m_position; }
	glm::vec3 getTarget() const { return m_target; }
	glm::vec3 getUp() const { return m_up; }
	float getSpeed() const { return m_speed; }
	float getSensitivity() const { return m_sensitivity; }

	virtual void moveForward() = 0;
	virtual void moveBack() = 0;
	virtual void moveLeft() = 0;
	virtual void moveRight() = 0;
	virtual void moveUp() = 0;
	virtual void moveDown() = 0;

	virtual void lookLeft(float dx) = 0;
	virtual void lookRight(float dx) = 0;
	virtual void lookUp(float dy) = 0;
	virtual void lookDown(float dy) = 0;

	virtual void updateCamera() = 0;

protected:
	glm::vec3 m_position;
	glm::vec3 m_target;
	glm::vec3 m_up;
	float m_speed;
	float m_sensitivity;
};

#endif