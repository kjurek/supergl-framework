#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glm/glm.hpp>
#include "Crosshair.h"

void Crosshair::render()
{
	glPushMatrix ();
	glLoadIdentity ();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix ();
	glLoadIdentity();
	GLint viewport [4];
	glGetIntegerv (GL_VIEWPORT, viewport);
	float width = (float)viewport[2];
	float height = (float)viewport[3];

	gluOrtho2D (0,width, height, 0);
	glDepthFunc (GL_ALWAYS);
	
	glColor4f(m_color.r, m_color.g, m_color.b, m_color.a);
	
	glBegin(GL_LINES);
		glLineWidth(100);
		glVertex2f(width / 2, (height/2) + 20);
		glVertex2f(width / 2, (height/2) + 5);
	glEnd();
	
	glBegin(GL_LINES);
		glLineWidth(100);
		glVertex2f(width / 2, (height/2) - 20);
		glVertex2f(width / 2, (height/2) - 5);
	glEnd();

	glBegin(GL_LINES);
	glLineWidth(100);

		glVertex2f( (width / 2) + 20, height / 2);
		glVertex2f( (width / 2) + 5, height / 2);
	glEnd();

	glBegin(GL_LINES);
	glLineWidth(100);

		glVertex2f( (width / 2) - 20, height / 2);
		glVertex2f( (width / 2) - 5, height / 2);
	glEnd();

	glDepthFunc (GL_LESS);
	glPopMatrix ();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

Cube Crosshair::toCube() const
{
	return Cube(glm::vec3(0,0,0), glm::vec3(0,0,0));
}