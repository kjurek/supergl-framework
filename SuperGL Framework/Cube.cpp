#include "Cube.h"
#include <iostream>
bool Cube::overlap(Cube other)
{
	/*
	from stackoverflow
	Cond1.  If A's left face is to the right of the B's right face,
	-  then A is Totally to right Of B
	CubeA.X2 < CubeB.X1
	Cond2.  If A's right face is to the left of the B's left face,
	-  then A is Totally to left Of B
	CubeB.X2 < CubeA.X1
	Cond3.  If A's top face is below B's bottom face,
	-  then A is Totally below B
	CubeA.Z2 < CubeB.Z1
	Cond4.  If A's bottom face is above B's top face,
	-  then A is Totally above B
	CubeB.Z2 < CubeA.Z1
	Cond5.  If A's front face is behind B's back face,
	-  then A is Totally behind B
	CubeA.Y1 < CubeB.Y2
	Cond6.  If A's back face is in front of B's front face,
	-  then A is Totally in front of B
	CubeB.Y2 < CubeA.Y1
	So the condition for no overlap is:

	Cond1 or Cond2 or Cond3 or Cond4 or Cond5 or Cond6
	Therefore, a sufficient condition for Overlap is the opposite (De Morgan)

	Not Cond1 AND Not Cond2 And Not Cond3 And Not Cond4 And Not Cond5 And Not Cond6
	*/

	/*

	bool cond1 = min_point.x > other.max_point.x;
	bool cond2 = max_point.x < other.min_point.x;
	bool cond3 = min_point.y > other.max_point.y;
	bool cond4 = max_point.y < other.min_point.y;
	bool cond5 = min_point.z > other.max_point.z;
	bool cond6 = max_point.z < other.min_point.z;

	if(cond1 || cond2 || cond3 || cond4 || cond5 || cond6)
		return false;
	else
	{
		std::cout << "A: ";
		std::cout << min_point.x << " " << min_point.y << " " << min_point.z << std::endl;
		std::cout << max_point.x << " " << max_point.y << " " << max_point.z << std::endl;

		std::cout << "b: ";
		std::cout << other.min_point.x << " " << other.min_point.y << " " << other.min_point.z << std::endl;
		std::cout << other.max_point.x << " " << other.max_point.y << " " << other.max_point.z << std::endl;

		return true;
	}
	*/

	return (max_point.x >= other.min_point.x && min_point.x <= other.max_point.x)
		&& (max_point.y >= other.min_point.y && min_point.y <= other.max_point.y)
		&& (max_point.z >= other.min_point.z && min_point.z <= other.max_point.z);
}