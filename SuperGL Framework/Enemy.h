#ifndef __ENEMY_H__
#define __ENEMY_H__

#include <Windows.h>
#include <cmath>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glm/glm.hpp>
#include "Renderable.h"
#include "Timer.h"

class Enemy : public Renderable
{
public:
	Enemy(glm::vec3* playerPos, glm::vec3 pos, int hp, float speed, float distance, float enemyRadius);
	virtual ~Enemy() { }

	glm::vec3 getPosition() const { return m_pos; }
	float getEnemyRadius() { return m_enemyRadius; }
	void move();
	virtual void render();
	virtual Cube toCube() const;

protected:
	glm::vec3* m_playerPos;
	glm::vec3 m_pos;
	int m_hp;
	float m_speed;
	float m_angle;
	float m_distance;
	float m_radius;
	float m_enemyRadius;
	Timer m_timer;
	GLUquadricObj* m_quadric;
	std::string textureName;
};

#endif