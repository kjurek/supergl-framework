#ifndef __HUD_H__
#define __HUD_H__

#include <string>
#include "Renderable.h"

class Hud : public Renderable
{
public:
	Hud(std::string text, glm::vec4 color, glm::vec3 position, unsigned int fontBase, bool second_dim, float scale);
	virtual ~Hud() { }

	virtual void render();
	virtual Cube toCube() const;

	void setText(std::string text) { m_text = text; }
	void setColor(glm::vec4 color) { m_color = color; }
	void setPosition(glm::vec3 position) { m_position = position; }
	void set2D(bool b) { m_2D = b; }

	std::string getText() const { return m_text; }
	glm::vec4 getColor() const { return m_color; }
	glm::vec3 getPosition() const { return m_position; }

private:
	std::string m_text;
	glm::vec4 m_color;
	glm::vec3 m_position;
	unsigned int m_fontBase;
	float m_scale;

	bool m_2D;
};
#endif