#include <ctime>
#include <algorithm>
#include "Logger.h"

bool Logger::addStream(std::ostream* stream)
{
	auto it = std::find(m_streams.begin(), m_streams.end(), stream);
	
	if(it != m_streams.end())
		return false;

	m_streams.push_back(stream);
	return true;
}

bool Logger::removeStream(std::ostream* stream)
{
	auto it = std::find(m_streams.begin(), m_streams.end(), stream);

	if(it == m_streams.end())
		return false;

	m_streams.erase(it);
	return true;
}

void Logger::beginMessage()
{
	time_t rawtime;
	time ( &rawtime );
	localtime ( &rawtime );
	
	for(std::ostream* stream : m_streams)
	{
		(*stream) << ctime(&rawtime);
	}
}