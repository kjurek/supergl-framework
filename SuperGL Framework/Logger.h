#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <list>
#include <iostream>

class Logger {
public:
	Logger() { }
	virtual ~Logger() { }

	bool addStream(std::ostream* stream);
	bool removeStream(std::ostream* stream);
	
	void beginMessage();
	
	template <typename T>
	friend Logger& operator <<(Logger& logger, const T& value)
	{
		for(std::ostream* stream : logger.m_streams)
			(*stream) << value;

		return logger;
	}

	template <typename T, size_t N>
	friend Logger& operator <<(Logger& logger, const T (&arr)[N]) {

		for(std::ostream* stream : logger.m_streams)
			(*stream) << arr;

		return logger;
	}

	friend Logger& operator<<( Logger& logger, std::ostream& (*f)( std::ostream& ) )
	{
		for(std::ostream* stream : logger.m_streams)
			f(*stream);

		return logger;
	}
private:
	std::list<std::ostream*> m_streams;
};

#endif