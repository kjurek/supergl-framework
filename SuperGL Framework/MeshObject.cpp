#include "MeshObject.h"

MeshObject::MeshObject() {
//	m_vbo = 0;
//	m_elements = 0;
}

void MeshObject::render() {
/*	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	//------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

	glNormalPointer(GL_FLOAT, 0, (void*)(m_elements*4));
	glVertexPointer(3, GL_FLOAT, 0, 0);
	glDrawArrays(GL_TRIANGLES, 0, m_elements);

	//------------------------------------------------
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY); */
	glBegin(GL_QUAD_STRIP);
	for(int i = 0; i < m_elements.size(); i++) {
		glColor3f(m_normals[m_elements[i]].x, m_normals[m_elements[i]].y, m_normals[m_elements[i]].z);	
		glNormal3f(m_normals[m_elements[i]].x, m_normals[m_elements[i]].y, m_normals[m_elements[i]].z);	
		glVertex3f(m_vertices[m_elements[i]].x, m_vertices[m_elements[i]].y, m_vertices[m_elements[i]].z);
	}
	glEnd();
	glBegin(GL_QUADS);
	for(int i = 0; i < m_elements.size(); i++) {
		glColor3f(m_normals[m_elements[i]].x, m_normals[m_elements[i]].y, m_normals[m_elements[i]].z);	
		glNormal3f(m_normals[m_elements[i]].x, m_normals[m_elements[i]].y, m_normals[m_elements[i]].z);	
		glVertex3f(m_vertices[m_elements[i]].x, m_vertices[m_elements[i]].y, m_vertices[m_elements[i]].z);
	}
	glEnd();
}

Cube MeshObject::toCube() const {
	return Cube(glm::vec3(0,0,0), glm::vec3(0,0,0));
}