#include "Player.h"

Bullet* Player::attack()
{
	glm::vec3 bullet_pos = m_pos;
	return new Bullet(bullet_pos, m_target, 3, 5.0f);
}