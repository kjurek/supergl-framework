#ifndef __RENDERABLE_H__
#define __RENDERABLE_H__

#include "Cube.h"

class Renderable
{
public:
	virtual void render() = 0;
	virtual Cube toCube() const = 0;
};
#endif

