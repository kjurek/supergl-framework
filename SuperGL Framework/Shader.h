#pragma once

#include <GL\GL.h>
#include <GL\GLU.h>

#include <string>
#include <memory>
#include <map>

using std::string;
using std::shared_ptr;
using std::map;
using std::pair;

class Shader {
private:
	shared_ptr<map<string, unsigned int>> m_shader;
	shared_ptr<map<string, unsigned int>> m_shaderProgram;
	Shader() : 
	m_shader(new map<string, unsigned int>()),
	m_shaderProgram(new map<string, unsigned int>()) {
	};
	~Shader() {
		m_shader->clear();
		m_shaderProgram->clear();
	};
public:
	typedef map<string, unsigned int>::iterator ShaderIterator;

	static Shader * getSingleton() {
		static Shader * p = new Shader();
		return p;
	}
	unsigned int getShaderId(const string& shaderKey) {
		ShaderIterator it = m_shader->find(shaderKey);
		if(it != m_shader->end())
			return it->second;
		return 0;
	};

	unsigned int getShaderProgramId(const string& shaderProgramKey) {
		ShaderIterator it = m_shaderProgram->find(shaderProgramKey);
		if(it != m_shaderProgram->end())
			return it->second;
		return 0;
	};

	bool saveShader(const string& shaderKey, unsigned int shaderId) {
		return m_shader->insert(pair<string,unsigned int>(shaderKey, shaderId)).second;
	};

	bool saveShaderProgram(const string& shaderProgramKey, unsigned int shaderProgramId) {
		return m_shaderProgram->insert(pair<string,unsigned int>(shaderProgramKey, shaderProgramId)).second;
	};

	void deleteShader(const string& shaderKey) {
		m_shader->erase(shaderKey);
	}

	void deleteShaderProgram(const string& shaderProgramKey) {
		m_shaderProgram->erase(shaderProgramKey);
	};

};