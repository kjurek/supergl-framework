#include <iostream>
#include <fstream>
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>


#include <gl/GLee.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <gl/glext.h>
#include <gl/wglext.h>

#include <string>

#include "ShaderManager.h"

#pragma comment(lib, "opengl32")
#pragma comment(lib, "glu32")

using std::ifstream;
using std::cout;
using std::endl;
using std::ios_base;

bool ShaderManager::loadShader(const string& path, const string& key, GLenum type) {
	ifstream s(path.c_str(), std::ifstream::in);
	if(s.is_open()) {
		char * content;
		if(s.good()) {
			s.seekg(0, ios_base::end);
			size_t size = s.tellg();
			s.seekg(0, ios_base::beg);
			
			content = new char[size+1];
			memset(content, 0, size + 1);
			s.read(content, size);

			cout << "shader: " << path.c_str() << endl;
			cout << content << endl;
			s.close();
			
			GLuint shader = glCreateShader(type);
			const char * shaderContent = content;
			glShaderSource(shader, 1, &shaderContent, nullptr);
			
			glCompileShader(shader); 
			int status;
			glGetShaderiv(shader,  GL_COMPILE_STATUS, &status);
			delete[] content;
			if(status != GL_TRUE) {
				MessageBox(nullptr, "Blad kompilacji shadera", "ShaderManager", MB_OK);
				PostQuitMessage(0);
			}
			return m_shaders->saveShader(key, shader);
		}
	}
	return 0;
};


void ShaderManager::createProgram(const string& programKey) {
	unsigned int programId = glCreateProgram();
	m_shaders->saveShaderProgram(programKey, programId);
}

void ShaderManager::attachShader(const string& key, const string& programKey) {
	unsigned int shaderId = m_shaders->getShaderId(key);
	unsigned int programId = m_shaders->getShaderProgramId(programKey);

	glAttachShader(programId, shaderId);
}


void ShaderManager::detachShader(const string& key, const string& programKey) {
	unsigned int shaderId = m_shaders->getShaderId(key);
	unsigned int programId = m_shaders->getShaderProgramId(programKey);

	glDetachShader(programId, shaderId);
}

void ShaderManager::useProgram(const string& programKey) {
	unsigned int programId = m_shaders->getShaderProgramId(programKey);
	glUseProgram(programId);
}

void ShaderManager::disableProgram() {
	glUseProgram(0);
}

void ShaderManager::linkProgram(const string& programKey) {
	unsigned int programId = m_shaders->getShaderProgramId(programKey);
	glLinkProgram(programId);
}

void ShaderManager::deleteShader(const string& key) {
	m_shaders->deleteShader(key);
}

void ShaderManager::deleteShaderProgram(const string& programKey) {
	m_shaders->deleteShaderProgram(programKey);
}

unsigned int ShaderManager::getShaderProgram(const string& programKey)
{
	return m_shaders->getShaderProgramId(programKey);
}