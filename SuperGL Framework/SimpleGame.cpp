#include <thread>
#include <ctime>
#include <cmath>
#include <iostream>
#include <sstream>
#include "SimpleGame.h"
#include "SphericalCamera.h"
#include "ShaderManager.h"

SimpleGame::SimpleGame(std::string title, size_t width, size_t height)
{
	m_gameState = SimpleGameStates::GAME_PAUSED;
	srand(time(0));
	m_window = new OpenGLWindow(title, width, height);
	m_window->create();
	m_player = new Player(m_window->m_pCamera->getPosition(), m_window->m_pCamera->getTarget(), 100);
	SetCursorPos(400, 300);
	GetCursorPos(&m_mousePosition);
	ScreenToClient(m_window->getHwnd(), &m_mousePosition);
	using namespace std::placeholders;
	m_window->addOnKeyDown(std::bind(&SimpleGame::onKeyDown, this, _1));
	m_window->addOnMouseMove(std::bind(&SimpleGame::onMouseMove, this, _1, _2));
	m_window->addOnMouseClick(std::bind(&SimpleGame::onMouseClick, this, _1, _2));
	m_window->addFunctionToLoop(std::bind(&SimpleGame::mainLoop, this));
	m_window->m_pCamera->setSpeed(1.0f);
	m_window->m_pCamera->setSensitivity(1.0f);
	m_enemyGeneratorThread = new std::thread(std::bind(&SimpleGame::generateEnemies, this));
	//TextureManager::getSingletion()->add("macierewicz", "..\\macierewicz.bmp");
	TextureManager::getSingletion()->add("ice", "ice.bmp");
	std::stringstream ss;
	for(int i = 0; i < 10; i++)
	{
		ss << i;
		std::string texName = ss.str();
		TextureManager::getSingletion()->add(texName, std::string("..\\") + texName + ".bmp");
		ss.str("");
	}
	//MeshManager::getSingleton()->add("monkey", "..\\monkey.obj");

	m_hpHud = new Hud("HP: 100%", glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), 
		glm::vec3(10, 40, 0), m_window->getDefaultFontBase(), true, 50.0f);
	m_window->registerRenderable(m_hpHud);
	m_hitPoint =  new Hud("Points: 0", glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), 
			glm::vec3(550, 40, 0), m_window->getDefaultFontBase(), true, 50.0f);
	m_window->registerRenderable(m_hitPoint);

	m_crosshair = new Crosshair(glm::vec4(0, 1.0f, 0, 0.5f));
	m_window->registerRenderable(m_crosshair);
	//m_window->registerRenderable(MeshManager::getSingleton()->get("monkey"));
	point = 0;
	/* chowa cursor */
	ShowCursor(false);

	ShaderManager::getSingleton()->loadShader("..\\vertexShader.vert", "simpleVert", GL_VERTEX_SHADER);
	ShaderManager::getSingleton()->loadShader("..\\fragmentShader.frag","simpleFrag", GL_FRAGMENT_SHADER);
	ShaderManager::getSingleton()->loadShader("..\\light.vert", "lightVert", GL_VERTEX_SHADER);
	ShaderManager::getSingleton()->loadShader("..\\light.frag","lightFrag", GL_FRAGMENT_SHADER);
	ShaderManager::getSingleton()->createProgram("simpleProgram");
	ShaderManager::getSingleton()->attachShader("lightVert", "simpleProgram");
	ShaderManager::getSingleton()->attachShader("lightFrag", "simpleProgram");
	ShaderManager::getSingleton()->attachShader("simpleVert", "simpleProgram");
	ShaderManager::getSingleton()->attachShader("simpleFrag", "simpleProgram");
	ShaderManager::getSingleton()->linkProgram("simpleProgram");

	m_window->runEventLoop();

}

SimpleGame::~SimpleGame()
{
	m_enemyGeneratorThread->join();

	delete m_enemyGeneratorThread;
	delete m_window;
	delete m_player;
	delete m_hpHud;
	delete m_crosshair;

	for(Bullet* it : m_bullets)
		delete it;

	for(Enemy* it : m_enemies)
		delete it;
}

void SimpleGame::onKeyDown(char key)
{
	switch (key)
	{
/*
// nie ma poruszania :D
	case VK_PRIOR:
		{
			m_window->m_pCamera->moveUp();
			m_player->setTarget(m_window->m_pCamera->getTarget());
			m_player->setPos(m_window->m_pCamera->getPosition());
		}
		break;
	case VK_NEXT:
		{
			m_window->m_pCamera->moveDown();
			m_player->setTarget(m_window->m_pCamera->getTarget());
			m_player->setPos(m_window->m_pCamera->getPosition());
		}
		break;
	case 'D':
		{
			m_window->m_pCamera->moveRight();
			m_player->setTarget(m_window->m_pCamera->getTarget());
			m_player->setPos(m_window->m_pCamera->getPosition());
		}
		break;
	case 'A':
		{
			m_window->m_pCamera->moveLeft();
			m_player->setTarget(m_window->m_pCamera->getTarget());
			m_player->setPos(m_window->m_pCamera->getPosition());
		}
		break;
	case 'W':
		{
			m_window->m_pCamera->moveForward();
			m_player->setTarget(m_window->m_pCamera->getTarget());
			m_player->setPos(m_window->m_pCamera->getPosition());
		}
		break;
	case 'S':
		{
			m_window->m_pCamera->moveBack();
			m_player->setTarget(m_window->m_pCamera->getTarget());
			m_player->setPos(m_window->m_pCamera->getPosition());
		}
		break;
		*/

	case VK_UP:
		{
			m_window->m_pCamera->lookUp(1.0f);
			m_player->setTarget(m_window->m_pCamera->getTarget());
		}
		break;
	case VK_DOWN:
		{
			m_window->m_pCamera->lookDown(1.0f);
			m_player->setTarget(m_window->m_pCamera->getTarget());
		}
		break;
	case VK_LEFT:
		{
			m_window->m_pCamera->lookLeft(1.0f);
			m_player->setTarget(m_window->m_pCamera->getTarget());
		}
		break;
	case VK_RIGHT:
		{
			m_window->m_pCamera->lookRight(1.0f);
			m_player->setTarget(m_window->m_pCamera->getTarget());
		}
		break;
	case VK_SPACE:
		{
			m_gameState = SimpleGameStates::GAME_STARTED;
		}
		break;
	default:
		break;
	}
}

void SimpleGame::onMouseMove(int x, int y)
{
	float dx = x - m_mousePosition.x;
	float dy = y - m_mousePosition.y;
	SphericalCamera* camera = reinterpret_cast<SphericalCamera*>(m_window->getCamera());
	if(dx > 0)
		camera->lookRight(dx);
	else if(dx < 0)
		camera->lookLeft(-dx);

	if(dy < 0)
	{
			camera->lookUp(-dy);
			//std::cout << camera->getVerticalAngle() << std::endl;
	}
	else if(dy > 0) {
			camera->lookDown(dy);
			//std::cout << camera->getVerticalAngle() << std::endl;
	}
	m_player->setTarget(camera->getTarget());
	SetCursorPos(400, 300);
}

void SimpleGame::onMouseClick(int x, int y)
{
	if(m_gameState == SimpleGameStates::GAME_STARTED)
	{
		Bullet* bullet = m_player->attack();

		m_window->getMutex().lock();
		m_bullets.push_back(bullet);
		m_window->registerRenderable(bullet);
		m_window->getMutex().unlock();
	}
}

void SimpleGame::mainLoop()
{
	m_window->getMutex().lock();
	bulletDestroy();
	enemyDestroy();
	detectCollision();
	m_window->getMutex().unlock();
}

void SimpleGame::bulletDestroy()
{
	for(auto it = m_bullets.begin(); it != m_bullets.end();)
	{
		Bullet* bullet = *it;
		if(abs(bullet->getPosition().z) >= 500)
		{
			if(m_window->unregisterRenderable(bullet))
			{
				it = m_bullets.erase(it);
				delete bullet;
			}
		} else {
			it++;
		}
	}
}

void SimpleGame::enemyDestroy()
{
	for(auto it = m_enemies.begin(); it != m_enemies.end();)
	{
		Enemy* enemy = *it;

		if(abs(enemy->getPosition().z - m_player->getPosition().z) <= 3)
		{
			m_player->setHp(m_player->getHp() - enemy->getEnemyRadius());
			std::stringstream ss;
			if(m_player->getHp() < 0)
				m_player->setHp(0);

			ss << "HP: " << m_player->getHp() << "%";
			m_hpHud->setText(ss.str());

			if(m_player->getHp() <= 0.0){
				Hud* m_HudGameOver = new Hud("Game Over", glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), 
			glm::vec3(175, 300, 0), m_window->getDefaultFontBase(), true, 100.0f);
				m_window->registerRenderable(m_HudGameOver);
				m_gameState = SimpleGameStates::GAME_PAUSED;
			}

			if(m_window->unregisterRenderable(enemy))
			{
				it = m_enemies.erase(it);
				delete enemy;
			}
		} else {
			it++;
		}
	}
}

void SimpleGame::detectCollision()
{
	bool overlap = false;

	for(auto bullet_it = m_bullets.begin(); bullet_it != m_bullets.end();)
	{
		Bullet* bullet = *bullet_it;
		for(auto enemy_it = m_enemies.begin(); enemy_it != m_enemies.end();)
		{
			Enemy* enemy = *enemy_it;

			overlap = enemy->toCube().overlap(bullet->toCube());
			
			if(overlap)
			{
				if(enemy->getEnemyRadius() < 20){
					point += enemy->getEnemyRadius();
				}else{
					point += enemy->getEnemyRadius()/2;
				}
				std::stringstream ss;
				ss << "Points: " << point;
				m_hitPoint->setText(ss.str());
				
				m_window->unregisterRenderable(enemy);
				enemy_it = m_enemies.erase(enemy_it);
				delete enemy;
				break;
			} else {
				enemy_it++;
			}
		}

		if(overlap)
		{

			m_window->unregisterRenderable(bullet);
			bullet_it = m_bullets.erase(bullet_it);
			delete bullet;
			overlap = false;
		} else {
			bullet_it++;
		}
	}
}

void SimpleGame::generateEnemies()
{
	while(m_gameState != SimpleGameStates::GAME_ENDED)
	{
		if(m_gameState == SimpleGameStates::GAME_STARTED)
		{
			glm::vec3 enemyPos;
			
			glm::vec3 player_pos = m_player->getPosition();
			glm::vec3 player_target = m_player->getTarget();
			glm::vec3 target_point = player_target + player_pos;
			float val = (rand()%10 + 1.0);
			enemyPos =  val * 100.0f * (target_point);
			
			if(rand()% 2 == 1){
				// dodatnie
				enemyPos.x += val / 1.5 * (rand() % 60 + 1);
			}else{
				// ujemne
				enemyPos.x -= val / 1.5 * (rand() % 60 + 1);
			}

			if(rand()% 2 == 1){
				// dodatnie
				enemyPos.y += val / 1.5 * (rand() % 60 + 1);
			}else{
				// ujemne
				enemyPos.y -= val / 1.5 * (rand() % 60 + 1);
			}

			float enemyRadius = rand() % 50 + 1;


			std::cout << "player position " << m_player->getPosition().x << " " << m_player->getPosition().y << std::endl;
			std::cout << "player target " << target_point.x << " " << target_point.y << " " << target_point.z << std::endl;

			std::cout << "creating enemy at " << enemyPos.x << " " << enemyPos.y << " " << enemyPos.z << std::endl;
			Enemy* enemy = new Enemy(&m_player->getPosition(), enemyPos, 20, 1.5f, val * 100.0f, enemyRadius+20);
			
			m_window->getMutex().lock();
				m_enemies.push_back(enemy);
				m_window->registerRenderable(enemy);
			m_window->getMutex().unlock();

			Sleep(4000);
		}
	}
}