#ifndef __SIMPLEGAME_H__
#define __SIMPLEGAME_H__

#include <list>
#include <mutex>
#include <Windows.h>
#include <cmath>
#include <gl/GLee.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glm/glm.hpp>
#include <vector>
#include "OpenGLWindow.h"
#include "TextureManager.h"
#include "ShaderManager.h"
#include "Renderable.h"

#include "Enemy.h"
#include "Bullet.h"
#include "Player.h"
#include "SimpleGameStates.h"
#include "Hud.h"
#include "Crosshair.h"

#include "MeshManager.h"

using namespace SuperGLFramework;

class SimpleGame
{
public:
	SimpleGame(std::string title, size_t width, size_t height);
	virtual ~SimpleGame();

protected:
	OpenGLWindow* m_window;
	Player* m_player;
	std::list<Enemy*> m_enemies;
	std::list<Bullet*> m_bullets;
	POINT m_mousePosition;
	SimpleGameStates m_gameState;
	std::thread* m_enemyGeneratorThread;
	Hud* m_hpHud;
	Hud* m_hitPoint;
	Crosshair* m_crosshair;
	int point;

	
	void onKeyDown(char key);
	void onMouseMove(int x, int y);
	void onMouseClick(int x, int y);

	void mainLoop();
	void bulletDestroy();
	void enemyDestroy();
	void detectCollision();
	void generateEnemies();
};
#endif