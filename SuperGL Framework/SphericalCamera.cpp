#include <cmath>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "SphericalCamera.h"

SphericalCamera::SphericalCamera()
	:	m_verticalAngle(0.0f), m_horizontalAngle(0.0f)
{ }

SphericalCamera::SphericalCamera(glm::vec3 position, glm::vec3 target, glm::vec3 up, 
				float speed, float sensitivity, float verticalAngle, float horizontalAngle)
				:	Camera(position, target, up, speed, sensitivity),
					m_verticalAngle(verticalAngle), m_horizontalAngle(horizontalAngle)
{ }


void SphericalCamera::moveForward()
{
	m_position += m_target * m_speed;
}

void SphericalCamera::moveBack()
{
	m_position -= m_target * m_speed;
}

void SphericalCamera::moveLeft()
{
	m_position.x -= m_speed;
}

void SphericalCamera::moveRight()
{
	m_position.x += m_speed;
}

void SphericalCamera::moveUp()
{
	m_position.y += m_speed;
}

void SphericalCamera::moveDown()
{
	m_position.y -= m_speed;
}

void SphericalCamera::lookLeft(float dx) 
{
	m_horizontalAngle -= m_sensitivity * dx;
}

void SphericalCamera::lookRight(float dx)
{
	m_horizontalAngle += m_sensitivity * dx;
}

void SphericalCamera::lookUp(float dy)
{
	m_verticalAngle -= m_sensitivity * dy;
}

void SphericalCamera::lookDown(float dy)
{
	m_verticalAngle += m_sensitivity * dy;
}

void SphericalCamera::updateCamera()
{
	const float rad = 3.14f / 180.0f;
	m_target.x = sin(m_horizontalAngle * rad) * cos(m_verticalAngle * rad);
	m_target.y = -sin(m_verticalAngle * rad);
	m_target.z = -cos(m_horizontalAngle * rad) * cos(m_verticalAngle * rad);
	
	glLoadIdentity();
	gluLookAt(m_position.x,m_position.y,m_position.z,
			  m_position.x+m_target.x, m_position.y+m_target.y,m_position.z+m_target.z, 
			  0.0, 1.0, 0.0);
}
