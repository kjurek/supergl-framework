#include <fstream>
#include <sstream>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/glext.h>
#include <iostream>
#include "TextureManager.h"
#include "TGALoader.h"

TextureManager* TextureManager::getSingletion()
{
	static TextureManager tm;
	return &tm;
}

bool TextureManager::enable(std::string key)
{
	auto it = m_container.find(key);
	if(it == m_container.end())
		return false;

	unsigned texture_id = it->second;
	glBindTexture(GL_TEXTURE_2D, texture_id);
	return true;
}

void TextureManager::disable()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

void TextureManager::add(std::string key, std::string path)
{
	auto it = m_container.find(key);
	if(it != m_container.end())
	{
		glDeleteTextures(1, &(it->second));
		m_container.erase(it);
	}

	unsigned texture_id;

	if(path.compare(path.length() - 3, 3, "tga") == 0)
	{
		std::cout << "loading tga";
		texture_id = loadTGATexture((char*)path.c_str());
	}
	else
		texture_id = loadBMPTexture(path);

	if(texture_id == 0)
		throw std::exception("could not load bmp texture");

	m_container.insert(std::make_pair(key,texture_id));
}

void TextureManager::remove(std::string key)
{
	auto it = m_container.find(key);
	if(it != m_container.end())
	{
		glDeleteTextures(1, &it->second);
		m_container.erase(it);
	}
}

unsigned TextureManager::loadBMPTexture(std::string path)
{
	unsigned texture_id = 0;
	std::ifstream in_file(path, std::ios_base::binary);

	if(!in_file)
		return 0;

	int tex_width = 0, tex_height = 0;
	std::ostringstream tex_buff_stream;
	
	in_file.seekg(18, in_file.beg);
	in_file.read(reinterpret_cast<char*>(&tex_width),4);
	in_file.read(reinterpret_cast<char*>(&tex_height),4);
	in_file.seekg(54, in_file.beg);
	tex_buff_stream << in_file.rdbuf();
	in_file.close();

	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, tex_width, tex_height, GL_BGR_EXT, GL_UNSIGNED_BYTE, tex_buff_stream.str().c_str());
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	return texture_id;
}

unsigned int TextureManager::loadTGATexture(char* path)
{
	Texture texture;
	unsigned int texture_id = 0;

	LoadTGA(&texture, path);
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, texture.width, texture.height, GL_BGR_EXT, GL_UNSIGNED_BYTE, texture.imageData);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	return texture_id;
}

unsigned int TextureManager::getTextureId(std::string key)
{
	return m_container.at(key);
}