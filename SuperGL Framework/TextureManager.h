#ifndef __TEXTURE_MANAGER_H__
#define __TEXTURE_MANAGER_H__

#include <unordered_map>
#include <string>

class TextureManager
{
public:
	static TextureManager* getSingletion();

	bool enable(std::string key);
	void disable();
	void add(std::string key, std::string path);
	void remove(std::string key);
	unsigned int getTextureId(std::string key);
private:
	TextureManager() { }
	TextureManager(const TextureManager&);
	TextureManager& operator=(const TextureManager&);

	unsigned loadBMPTexture(std::string path);
	unsigned loadTGATexture(char* path);

	std::unordered_map<std::string, unsigned> m_container;
};

#endif

