#include <windows.h>
#include "Timer.h"

Timer::Timer()
	:	m_currentTime(0)
{ }

Timer::~Timer() { }

void Timer::start()
{
	m_currentTime = GetTickCount();
}

unsigned long Timer::getTimer()
{
	return GetTickCount() - m_currentTime;
}