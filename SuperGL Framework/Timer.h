#ifndef __TIMER_H__
#define __TIMER_H__

class Timer
{
public:
	Timer();
	virtual ~Timer();

	void start();
	unsigned long getTimer();

protected:
	unsigned long m_currentTime;
};
#endif