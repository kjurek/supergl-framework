#define WIN32_LEAN_AND_MEAN
#include <iostream>
#include <Windows.h>
#include <thread>
#include <string>
#include <gl/GLee.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include "OpenGLWindow.h"
#include "TextureManager.h"
#include "ShaderManager.h"
#include "Renderable.h"

using namespace std;
using namespace SuperGLFramework;

class Sphere : public Renderable
{
public:
	Sphere(int radius, glm::vec3 pos)
		:	m_radius(radius), m_pos(pos)
	{
		m_quadric = gluNewQuadric();
		gluQuadricDrawStyle(m_quadric, GLU_FILL);
		gluQuadricNormals(m_quadric, GLU_SMOOTH);
		gluQuadricOrientation(m_quadric, GLU_OUTSIDE);
		gluQuadricTexture(m_quadric, GL_TRUE);
		ShaderManager::getSingleton()->loadShader("vertexShader.vert", "simpleVert", GL_VERTEX_SHADER);
		ShaderManager::getSingleton()->loadShader("fragmentShader.frag","simpleFrag", GL_FRAGMENT_SHADER);
		ShaderManager::getSingleton()->loadShader("light.vert", "lightVert", GL_VERTEX_SHADER);
		ShaderManager::getSingleton()->loadShader("light.frag","lightFrag", GL_FRAGMENT_SHADER);
		ShaderManager::getSingleton()->createProgram("simpleProgram");
		ShaderManager::getSingleton()->attachShader("lightVert", "simpleProgram");
		ShaderManager::getSingleton()->attachShader("lightFrag", "simpleProgram");
		ShaderManager::getSingleton()->attachShader("simpleVert", "simpleProgram");
		ShaderManager::getSingleton()->attachShader("simpleFrag", "simpleProgram");
		ShaderManager::getSingleton()->linkProgram("simpleProgram");
	}


	virtual void render()
	{
		glTranslatef(m_pos.x, m_pos.y, m_pos.z);
		ShaderManager::getSingleton()->useProgram("simpleProgram");
		gluSphere(m_quadric,m_radius, 32, 32);
		ShaderManager::getSingleton()->disableProgram();
	}
private:
	GLUquadricObj* m_quadric;
	int m_radius;
	glm::vec3 m_pos;
};


class Wall : public Renderable
{
public:
	Wall(std::string texture_name, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4)
		:	texture_name(texture_name), p1(p1), p2(p2), p3(p3), p4(p4) 
	{ }

	virtual void render() 
	{
		glTranslatef(0,0,-20);
		TextureManager::getSingletion()->enable(texture_name.c_str());
		glBegin(GL_QUADS);
		
		glTexCoord3i(p1.x, p1.y, p1.z);
		glVertex3i(p1.x, p1.y, p1.z);

		glTexCoord3i(p2.x, p2.y, p2.z);
		glVertex3i(p2.x, p2.y, p2.z);

		glTexCoord3i(p3.x, p3.y, p3.z);
		glVertex3i(p3.x, p3.y, p3.z);

		glTexCoord3i(p4.x, p4.y, p4.z);
		glVertex3i(p4.x, p4.y, p4.z);

		glEnd();
		TextureManager::getSingletion()->disable();
	}

private:
	glm::vec3 p1, p2, p3, p4;
	std::string texture_name;
};

class SomeClient
{
public:
	SomeClient(std::string title)
		:	m_window(title)
	{
		m_window.create();
		TextureManager::getSingletion()->add("earth","earth.bmp");
		TextureManager::getSingletion()->add("macierewicz","macierewicz.bmp");
		SetCursorPos(400, 300);
		GetCursorPos(&m_mousePosition);
		ScreenToClient(m_window.getHwnd(), &m_mousePosition);
		using namespace placeholders;
		m_window.addOnKeyDown(std::bind(&SomeClient::onKeyDown, this, _1));
		m_window.addOnMouseMove(std::bind(&SomeClient::onMouseMove, this, _1, _2));
		m_wall[0] = new Wall("earth", glm::vec3(0, 0, 0), glm::vec3(100, 0, 0), glm::vec3(100, 100, 0), glm::vec3(0, 100, 0));
		m_wall[1] = new Wall("macierewicz", glm::vec3(0, 0, -10), glm::vec3(50, 0, -10), glm::vec3(50, 50, -10), glm::vec3(0, 50, -10));
		m_sphere[0] = new Sphere(10, glm::vec3(1, 0, 0));
		m_sphere[1] = new Sphere(5, glm::vec3(-10, 0, 0));
		m_window.registerRenderable(m_sphere[0]);
		m_window.registerRenderable(m_sphere[1]);
		m_window.registerRenderable(m_wall[0]);
		m_window.registerRenderable(m_wall[1]);
		m_window.m_pCamera->setSpeed(1.0f);
		m_window.m_pCamera->setSensitivity(1.0f);
		m_window.runEventLoop();
	}

	void onKeyDown(char key)
	{
		switch (key)
		{
		case VK_PRIOR:
			{
				m_window.m_pCamera->moveUp();
			}
			break;
		case VK_NEXT:
			{
				m_window.m_pCamera->moveDown();
			}
			break;
		case 'D':
			{
				m_window.m_pCamera->moveRight();
			}
			break;
		case 'A':
			{
				m_window.m_pCamera->moveLeft();
			}
			break;
		case 'W':
			{
				m_window.m_pCamera->moveForward();
			}
			break;
		case 'S':
			{
				m_window.m_pCamera->moveBack();
			}
			break;
		case VK_UP:
			{
				m_window.m_pCamera->lookUp(1.0f);
			}
			break;
		case VK_DOWN:
			{
				m_window.m_pCamera->lookDown(1.0f);			
			}
			break;
		case VK_LEFT:
			{
				m_window.m_pCamera->lookLeft(1.0f);
			}
			break;
		case VK_RIGHT:
			{
				m_window.m_pCamera->lookRight(1.0f);
			}
			break;
		default:
			break;
		}
	}
	
	void onMouseMove(int x, int y)
	{
		float dx = x - m_mousePosition.x;
		float dy = y - m_mousePosition.y;
		
		if(dx > 0)
			m_window.m_pCamera->lookRight(dx);
		else if(dx < 0)
			m_window.m_pCamera->lookLeft(-dx);

		if(dy < 0)
			m_window.m_pCamera->lookUp(-dy);
		else if(dy > 0)
			m_window.m_pCamera->lookDown(dy);

		SetCursorPos(400, 300);
	}

private:	
	OpenGLWindow m_window;
	Wall* m_wall[2];
	Sphere* m_sphere[2];
	POINT m_mousePosition;

};

	void launchClient(std::string title)
	{
		SomeClient client(title);
	}

	int main(int argc, char** argv)
	{
		launchClient("client");
		return 0;
	}