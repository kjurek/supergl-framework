varying vec3 normal,lightDir,halfVector;

vec3 n,halfV;
vec4 color;
float NdotL,NdotHV;

void enableLightFrag()
{
	//color = gl_LightSource[0].ambient * gl_FrontMaterial.ambient;
	n = normalize(normal);
	NdotL = max(dot(n,lightDir),0.0);

	//if (NdotL > 0.0)
	//{
		color = gl_LightSource[0].diffuse *
		gl_FrontMaterial.diffuse * NdotL;
		halfV = normalize(halfVector);
		NdotHV = max(dot(n,halfV),0.0);
	//}
	
	color *= gl_LightSource[0].specular;
	gl_FragColor += color;
}